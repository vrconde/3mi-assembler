# 3MI Assembler

## Installation
```bash
conda create --name merge_3mi_env python=3.8 netcdf4=1.5.7
conda activate merge_3mi_env
```


## Usage
```bash
python merge_3mi.py
```
