# conda activate merge_3mi_env

import netCDF4



def overlaps_pdu2pdu(source, destination, overlaps_group='data'):

    """It copies all overlaps from source 3MI PDU to destination 3MI PDU

    ARGUMENTS:
        source {string} -- Source 3MI PDU file path.
        destination {string} -- Destination 3MI PDU file path.
        overlaps_group {string} -- NetCDF Group name containing the overlaps.
    """


    with netCDF4.Dataset(source, 'r') as src, netCDF4.Dataset(destination, 'a') as dst:

        src.set_auto_scale(False)
        dst.set_auto_scale(False)

        """
        class lol:
            size = 4
        lolobj = lol()

        for dim_name, dimension in dst.dimensions.items():
            if (dim_name == "overlaps"):
                dimension  = lolobj
                dst.dimensions[dim_name] = lolobj
                print("  v:" + dim_name)
                print(dimension.size)

        for dim_name, dimension in dst.dimensions.items():
            if (dim_name == "overlaps"):
                print("  v:" + dim_name)
                print(dimension.size)
        """

        # overlap_001, overlap_002, ...
        for name01, group01 in src[overlaps_group].groups.items():
            
            print("g:" + name01)
            overlap_group_name = name01
            
            if (overlap_group_name.split('_')[1] == '000'):
                overlap_group_name = overlap_group_name.split('_')[0] + '_002'
            else:
                overlap_group_name = overlap_group_name.split('_')[0] + '_003'
            
            overlap_group = dst[overlaps_group].createGroup(overlap_group_name)

            for dim_name02, dimension02 in group01.dimensions.items():
                print(" d:" + dim_name02)
                overlap_group.createDimension(dim_name02, (len(dimension02) if not dimension02.isunlimited() else None))

            for var_name02, variable02 in group01.variables.items():
                print(" v:" + var_name02)
                overlap_group.createVariable(var_name02, variable02.datatype, variable02.dimensions)
                overlap_group[var_name02][:] = group01[var_name02][:]
                ####### copy variable attributes all at once via dictionary
                overlap_group[var_name02].setncatts(group01[var_name02].__dict__)

            for name02, group02 in group01.groups.items():
                
                print(" g:" + name02)
                dst_group02 = overlap_group.createGroup(name02)

                for dim_name03, dimension03 in group02.dimensions.items():
                    print("  d:" + dim_name03)
                    dst_group02.createDimension(dim_name03, (len(dimension03) if not dimension03.isunlimited() else None))

                for var_name03, variable03 in group02.variables.items():
                    print("  v:" + var_name03)
                    dst_group02.createVariable(var_name03, variable03.datatype, variable03.dimensions)
                    dst_group02[var_name03][:] = group02[var_name03][:]
                    ####### copy variable attributes all at once via dictionary
                    dst_group02[var_name03].setncatts(group02[var_name03].__dict__)

                for name03, group03 in group02.groups.items():
                    
                    print("  g:" + name03)
                    dst_group03 = dst_group02.createGroup(name03)

                    for dim_name04, dimension04 in group03.dimensions.items():
                        print("   d:" + dim_name04)
                        dst_group03.createDimension(dim_name04, (len(dimension04) if not dimension04.isunlimited() else None))

                    for var_name04, variable04 in group03.variables.items():
                        print("   v:" + var_name04)
                        #dst_group03.createVariable(var_name04, variable04.datatype, variable04.dimensions) #'f4'
                        dst_group03.createVariable(var_name04, variable04.datatype, variable04.dimensions)
                        dst_group03[var_name04][:] = group03[var_name04][:]
                        print(dst_group03[var_name04][:])
                        print(variable04.datatype)
                        #print(type(group03[var_name04][2]))
                        #print(dst_group03[var_name04][2])
                        ####### copy variable attributes all at once via dictionary
                        dst_group03[var_name04].setncatts(group03[var_name04].__dict__)

                    for name04, group04 in group03.groups.items():
                    
                        print("   g:" + name04)
        

def main ():
    
    nc3mi1 = './3MI_1.nc'
    nc3mi2 = './3MI_2.nc'
    
    overlaps_pdu2pdu(nc3mi1, nc3mi2)


if __name__ == "__main__":
    
    main ()
